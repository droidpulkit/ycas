package gs.com.ycas.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import gs.com.ycas.BaseActivity;
import gs.com.ycas.R;

public class LauncherActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        final Context context = this;
        Thread delay = new Thread(){
            public  void run(){
                try {
                    sleep(3000);
                } catch (Exception e){
                    Toast.makeText(context, "Something went wrong...", Toast.LENGTH_SHORT).show();
                } finally {
                    checkLogin();
                }
            }
        };
        delay.start();
    }

    void checkLogin(){
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null){
            String uid = user.getUid();

            SharedPreferences sharedPref = getSharedPreferences("userData", MODE_PRIVATE);
            String blindOrVolunteer = sharedPref.getString("blindOrVolunteer", "Blind");

            if (blindOrVolunteer.equals("Blind")){
                Intent intent = new Intent(LauncherActivity.this, HomeActivity.class);
                startActivity(intent);
                finish();
            } else {
                Intent intent = new Intent(LauncherActivity.this, VolunteerHomeActivity.class);
                startActivity(intent);
                finish();
            }

        } else {
            Intent intent = new Intent(LauncherActivity.this, LandingActivity.class);
            startActivity(intent);
            finish();
        }
    }
}
