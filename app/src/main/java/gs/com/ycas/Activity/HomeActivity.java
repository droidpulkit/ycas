package gs.com.ycas.Activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.gson.Gson;
import com.microsoft.projectoxford.vision.VisionServiceClient;
import com.microsoft.projectoxford.vision.VisionServiceRestClient;
import com.microsoft.projectoxford.vision.contract.AnalysisResult;
import com.microsoft.projectoxford.vision.contract.Caption;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.Locale;

import gs.com.ycas.BaseActivity;
import gs.com.ycas.R;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String TAG = HomeActivity.class.getSimpleName();

    static final int REQUEST_IMAGE_CAPTURE = 1;

    TextToSpeech t1;

    Button takePic, captureVideo;
    ImageView imageView;

//    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
//    String uid = "";
//
//    private StorageReference mStorageRef;
//
//    private Uri mImageUri = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

//        mStorageRef = FirebaseStorage.getInstance().getReference();
//
//        if (user != null) {
//            uid = user.getUid();
//        }

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        takePic = findViewById(R.id.btn_take_pic);
        captureVideo = findViewById(R.id.btn_take_video);
        imageView = findViewById(R.id.imageView);
        takePic.setOnClickListener(this);
        captureVideo.setOnClickListener(this);

        t1 = new TextToSpeech(HomeActivity.this, new TextToSpeech.OnInitListener() {

            @Override
            public void onInit(int status) {
                if(status != TextToSpeech.ERROR) {
                    t1.setLanguage(Locale.UK);
                }
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_take_pic:
                Intent intent = new Intent(HomeActivity.this, TestActivity.class);
                startActivity(intent);
                break;
            case R.id.btn_take_video:
                Intent intent2 = new Intent(HomeActivity.this, MainActivity.class);
                startActivity(intent2);
                break;
            default:
                break;
        }
    }
}