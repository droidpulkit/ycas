package gs.com.ycas.Activity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.google.gson.Gson;
import com.microsoft.projectoxford.vision.VisionServiceClient;
import com.microsoft.projectoxford.vision.VisionServiceRestClient;
import com.microsoft.projectoxford.vision.contract.AnalysisResult;
import com.microsoft.projectoxford.vision.contract.Caption;
import com.otaliastudios.cameraview.CameraListener;
import com.otaliastudios.cameraview.CameraUtils;
import com.otaliastudios.cameraview.CameraView;
import com.otaliastudios.cameraview.Gesture;
import com.otaliastudios.cameraview.GestureAction;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.Locale;

import gs.com.ycas.R;

public class TestActivity extends AppCompatActivity {

    CameraView cameraView;

    TextToSpeech t1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        cameraView = findViewById(R.id.camera);
        cameraView.mapGesture(Gesture.TAP, GestureAction.CAPTURE); // one tap to shoot!

        cameraView.addCameraListener(new CameraListener() {

            CameraUtils.BitmapCallback bitmapCallback;
            @Override
            public void onPictureTaken(byte[] picture) {

                Log.d("TestActivity", "onPictureTaken: worked");

                Bitmap bitmap = BitmapFactory.decodeByteArray(picture, 0, picture.length);

                startImageRecognition(bitmap);

                // Create a bitmap or a file...
                // CameraUtils will read EXIF orientation for you, in a worker thread.
                //CameraUtils.decodeBitmap(picture,...);
            }
        });

        t1 = new TextToSpeech(TestActivity.this, new TextToSpeech.OnInitListener() {

            @Override
            public void onInit(int status) {
                if(status != TextToSpeech.ERROR) {
                    t1.setLanguage(Locale.UK);
                }
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
        cameraView.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        cameraView.stop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        cameraView.destroy();
    }

    public static long bytesToMeg(long bytes) {

        return bytes / (1024L * 1024L );
    }

    void startImageRecognition(Bitmap bitmap){
        String uriBase = "https://canadacentral.api.cognitive.microsoft.com/vision/v2.0/describe?";
        final VisionServiceClient visionServiceClient = new VisionServiceRestClient("ff00afa0014e487e8f24bf5086a7b57b", uriBase);

        final AsyncTask<Bitmap, String, String> visionTask = new AsyncTask<Bitmap, String, String>() {
            @Override
            protected String doInBackground(Bitmap... params) {
                try {

                    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                    params[0].compress(Bitmap.CompressFormat.JPEG,10, outputStream);
                    ByteArrayInputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());

                    byte[] imageInByte = outputStream.toByteArray();
                    long lengthbmp = imageInByte.length;
                    long ans = bytesToMeg(lengthbmp);


                    String[] features = {"Description"};
                    String[] details = {};



                        AnalysisResult result = visionServiceClient.analyzeImage(inputStream, features, details );


                    String strResult = "";
                    if(result == null){
                        strResult = "Please take another picture";
                    }else{
                        strResult = new Gson().toJson(result);
                    }

                    return strResult;
                } catch (Exception e){
                    e.printStackTrace();
                    return null;
                }
            }

            @Override
            protected void onPostExecute(String s) {
                AnalysisResult result = new Gson().fromJson(s, AnalysisResult.class);
                //TextView detail = findViewById(R.id.imgDetail);
                StringBuilder stringBuilder = new StringBuilder();

                if (result != null){
                    for (Caption caption: result.description.captions){
                        stringBuilder.append(caption.text);
                    }

                    tts(stringBuilder.toString());
                }else{
                    tts("Sorry, we can not identify this right now.");
                }

                //detail.setText(stringBuilder);
            }
        };

        visionTask.execute(bitmap);
    }

    void tts(String toSpeak){
        t1.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, null);
    }


}
